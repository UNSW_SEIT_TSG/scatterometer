# UNSW SEIT Scatterometer

This repository is now only for reference.  The system has been divided into two new repositories:
* [scatterometer_gui](https://gitlab.com/UNSW_SEIT_TSG/scatterometer_gui); and
* [scatterometer_server](https://gitlab.com/UNSW_SEIT_TSG/scatterometer_server)

The intended configuration is shown below.

![Alt text](/docs/system/system.png?raw=true "System diagram")

Two other repositories exist for debugging and development purposes:
* [scatterometer_cli](https://gitlab.com/UNSW_SEIT_TSG/scatterometer_cli) which is a command line tool for interacting with the server manually; and
* [scatterometer_virtual_xpsc8](https://gitlab.com/UNSW_SEIT_TSG/scatterometer_virtual_xpsc8) which can be used for testing or development when the actual controller or hardware is not available.

Some files from before the migration have been retained in this repository, however most content has been moved to the new destinations or replaced by new code.

# System Purpose

Enable efficient characterisation of a [BRDF](https://en.wikipedia.org/wiki/Bidirectional_reflectance_distribution_function) given some sample/specimen.
Some images from the lab are available [here](https://photos.app.goo.gl/k6yNsfBEPHH7bt7S2).

## Development Video Blog Updates
### Playlist
https://www.youtube.com/playlist?list=PLVNS53Nt-cACsvPfNtGkGE-osMXboKqex
### Entries
=======
- 2019 February  21 https://youtu.be/qpx6Ld4Co4Y
- 2018 October   18 https://youtu.be/8WdJBwf_vak Motor Control Scene and Communications with scaled down server.
- 2018 September 03 https://youtu.be/KGY56IXlCJI Externalised data from application code, as well as externalised XPSC8 specific relay function calls.
- 2018 August    22 https://youtu.be/BlNejiwxGIg Control multiple stages simultaneously and asynchronously from GUI (Part 2); Implemented consistent meaningful colours across 2D and 3D viewport content
- 2018 August    20 https://youtu.be/vLPufQYxLGo Control multiple stages simultaneously and asynchronously from GUI (Part 1)
- 2018 August    17 https://youtu.be/lKCIY4BIkoE Reboot button and axis labels done.
- 2018 August    16 https://youtu.be/7_oaBUflRhc Remote sensing via revised async relay server.
- 2018 July      27 https://youtu.be/dJo9JputLQw GUI to Sensor comms via: Network to Serial and back again for collection of readings etc.
- 2018 July      19 https://youtu.be/XzI5ujylrt4 Coloured axes added, ability to lock to north pole, and controlling camera using mouse.
- 2018 July      17 https://youtu.be/w_dOYrXkGgY Uniform random distribution of 3D point hemisphere with sample
- 2018 July      05 https://youtu.be/S_Ccjy0nraU Dynamic 2D arc linked to angle sliders.
- 2018 July      03 https://youtu.be/x7IH2wJAMZE Heartbeat, auto-reconnect, basic 3D.
- 2018 July      02 https://youtu.be/o_DIBdypjjc
- 2018 June      22 https://youtu.be/xLAxq6ZWiFo
- 2018 June      14 https://youtu.be/yWM4t5FgJJ4
- 2018 June      13 https://youtu.be/6V6S8Yi7a3s
- 2018 April     27 No Entry, but physical hardware stack mostly rebuilt + some coding.
- 2018 April     26 https://youtu.be/Izt9LTR49dQ
- 2018 April     24 https://youtu.be/lAuZW3k8gHI
- 2018 April     23 https://youtu.be/6yEgU6x9hzU
- 2018 April     20 https://youtu.be/XLe1Ddf_wQQ
- 2018 April     19 https://youtu.be/v3V95vwd4ZU
- 2018 April     18 https://youtu.be/swlPDn7OZA0
- 2018 April     17 https://youtu.be/pqnWK9phjUg

## Hardware Configuration
From bottom to top (A to G) in order of mechanical attachment:
- A: RV240CCHL    : XPSDRV03 : Plug 1 (Lowest in stack)
- B: RV160CCHL    : XPSDRV03 : Plug 2
- C: M-MTM250CC.1 : XPSDRV01 : Plug 3
- D: M-IMS300CCHA : XPSDRV03 : Plug 4
- E: RV120CCHL    : XPSDRV03 : Plug 5
- F: M-IMS100V    : XPSDRV01 : Plug 6
- G: RVS80CC      : XPSDRV01 : Plug 7 (Highest in stack)

## Reference frames
Natural_z = axis perpendicular to plane of benchtop analogous to axis of gravity; positive being away from the earth.
Default linear unit: 'millimetre' denoted [mm].
Default angular unit: 'degree' denotes [°].

## Movement Chains
- A: Independent (Rotation about Natural_z)
- B: C ()
- C: D ()
- D: E ()
- E: F ()
- F: G ()
- G: Sample ()

## Movement Ranges
### A: RV240CCHL
* MaximumTargetPosition: 170 [°];
* MinimumTargetPosition: -170 [°]; 
### B: RV160CCHL
* MaximumTargetPosition: 170 [°];
* MinimumTargetPosition: -170 [°];
### C: M-MTM250CC.1
* MaximumTargetPosition: 125 [mm];
* MinimumTargetPosition: -125 [mm];
### D: M-IMS300CCHA
* MaximumTargetPosition: 300 [mm];
* MinimumTargetPosition: 0 [mm];
### E: RV120CCHL
* MaximumTargetPosition: 170 [°];
* MinimumTargetPosition: -170 [°];
### F: M-IMS100V
* MaximumTargetPosition: 92 [mm];
* MinimumTargetPosition: ; -8 [mm]
### G: RVS80CC
* MaximumTargetPosition: 170 [°];
* MinimumTargetPosition: -170 [°];

## Contact
- j.forbes@adfa.edu.au. 
