
description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	400
Start Data Timestamp: 2018-Jun-27 16:38:51
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(8.620971000000002e-09, 1.452323169959078e-11)
1	-199.9	(8.611363000000002e-09, 1.1457553447398862e-11)
End Data Timestamp: 2018-Jun-27 16:38:51


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	450
Start Data Timestamp: 2018-Jun-27 16:39:17
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(3.152596000000001e-06, 1.0548544164954708e-08)
1	-199.9	(3.1535079999999998e-06, 1.1289797872415618e-08)
End Data Timestamp: 2018-Jun-27 16:39:17


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	500
Start Data Timestamp: 2018-Jun-27 16:39:44
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(4.579211e-06, 2.1945559436934007e-08)
1	-199.9	(4.576095e-06, 2.4967179956895455e-08)
End Data Timestamp: 2018-Jun-27 16:39:44


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	550
Start Data Timestamp: 2018-Jun-27 16:40:11
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(5.0876260000000005e-06, 2.1278616590370716e-08)
1	-199.9	(5.076548e-06, 2.08972557049963e-08)
End Data Timestamp: 2018-Jun-27 16:40:11


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	600
Start Data Timestamp: 2018-Jun-27 16:40:38
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(4.130091999999999e-06, 2.3920245316467775e-08)
1	-199.9	(4.1304610000000005e-06, 2.2830426605738225e-08)
End Data Timestamp: 2018-Jun-27 16:40:38


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	650
Start Data Timestamp: 2018-Jun-27 16:41:04
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(3.3439680000000005e-06, 2.010632676547359e-08)
1	-199.9	(3.334205000000001e-06, 1.803166312351692e-08)
End Data Timestamp: 2018-Jun-27 16:41:04


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	700
Start Data Timestamp: 2018-Jun-27 16:41:30
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(2.344024e-06, 1.1703590218390253e-08)
1	-199.9	(2.338942e-06, 1.5223752362673288e-08)
End Data Timestamp: 2018-Jun-27 16:41:30


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	750
Start Data Timestamp: 2018-Jun-27 16:41:58
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(1.967542e-06, 9.50544244104397e-09)
1	-199.9	(1.9699790000000003e-06, 1.3364492470722546e-08)
End Data Timestamp: 2018-Jun-27 16:41:58


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	800
Start Data Timestamp: 2018-Jun-27 16:42:24
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(1.3573580000000002e-06, 1.1216525130360116e-08)
1	-199.9	(1.356418e-06, 1.0534480338393541e-08)
End Data Timestamp: 2018-Jun-27 16:42:24


description:	Spectralon plate reference for near perfect base reflectivity. To be used to calibrate the black and white plastic and Z306 coated plate reflectivities

wavelength:	850
Start Data Timestamp: 2018-Jun-27 16:42:51
Total readings being taken, N = 2
nth reading: n
stage_position: s
optical_reading: o
n	s	o
0	-199.8	(5.72208e-07, 2.600833712485283e-09)
1	-199.9	(5.716240000000001e-07, 3.0280066050126094e-09)
End Data Timestamp: 2018-Jun-27 16:42:51

